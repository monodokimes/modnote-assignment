﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace modnote_assignment {
    public partial class AddModuleForm : Form {

        public string Title { get; set; }
        public string Code { get; set; }
        public string Synopsis { get; set; }

        public AddModuleForm() {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e) {
            Title = titleTextBox.Text;
            Code = codeTextBox.Text;
            Synopsis = synopsisTextBox.Text;

            DialogResult = DialogResult.OK;
        }
    }
}
