﻿using ModNote.ModNote;
using modnote_assignment;
using System.Collections.Generic;
using System.IO;

namespace ModNote {
    public class ModNoteManager {
        #region Singleton
        static readonly ModNoteManager mInstance = new ModNoteManager();

        static ModNoteManager() { }

        private ModNoteManager() {
            _saveDataManager = new SaveDataManager(SaveFilename);

            if (!File.Exists(SaveFilename)) {
                var textDataManager = new TextDataManager();
                textDataManager.LoadData();

                SetModules(textDataManager.Modules);
                SaveModules();
            }
            else {
                _saveDataManager.LoadModules();
            }
        }

        public static ModNoteManager Instance => mInstance;
        #endregion

        const string SaveFilename = "save.dat";

        readonly SaveDataManager _saveDataManager;

        public List<Module> Modules => _saveDataManager.Modules;

        public void SetModules(IEnumerable<Module> modules) => _saveDataManager.SetModules(modules);

        public void SaveModules() => _saveDataManager.SaveModules();
    }
}
