﻿using ModNote.ModNote;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.Serialization.Formatters.Binary;
using System.Text;
using System.Threading.Tasks;

namespace modnote_assignment {
    public class SaveDataManager {
        private List<Module> _modules;
        public List<Module> Modules => _modules;
        readonly BinaryFormatter _formatter = new BinaryFormatter();

        private string _filename;

        public SaveDataManager(string filename) {
            _filename = filename;
        }

        public void SetModules(IEnumerable<Module> modules) => _modules = modules.ToList();
        
        public void ClearSaveData() => File.Delete(_filename);

        public void SaveModules() {
            using (FileStream fileStream = new FileStream(_filename, FileMode.Create, FileAccess.Write))
                _formatter.Serialize(fileStream, _modules);
        }

        public void LoadModules() {
            if (File.Exists(_filename))
                using (FileStream fileStream = new FileStream(_filename, FileMode.Open, FileAccess.Read))
                   _modules = ((IEnumerable<Module>)_formatter.Deserialize(fileStream)).ToList();
        }
    }
}
