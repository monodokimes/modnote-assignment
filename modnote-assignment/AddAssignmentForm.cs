﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace modnote_assignment {
    public partial class AddAssignmentForm : Form {

        public DateTime DueDate { get; set; }
        public string Description { get; set; }

        public AddAssignmentForm() {
            InitializeComponent();
        }

        private void addAssignmentButton_Click(object sender, EventArgs e) {
            DueDate = dueDateDateTimePicker.Value;
            Description = descriptionTextBox.Text;

            DialogResult = DialogResult.OK;
        }
    }
}
