﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ModNote.ModNote {
    [Serializable]
    public class Module : ModNoteObject {
        public string Code { get; set; }
        public string Title { get; set; }
        public string Synopsis { get; set; }
        public List<LearningObjective> LearningObjectives { get; set; }
        public List<Assignment> Assignments { get; set; }
        public List<Note> Notes { get; set; }

        public override void AddNote(Note note) {
            Notes.Add(note);
        }

        public override void RemoveNote(Guid guid) {
            Notes = Notes.Where(n => n.Guid != guid).ToList();
        }
    }
}
