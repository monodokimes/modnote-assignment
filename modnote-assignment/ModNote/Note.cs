﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ModNote.ModNote {
    [Serializable]
    public class Note : ModNoteObject {
        public string Title { get; set; }
        public string Text { get; set; }
        public List<string> Links { get; set; }
        public List<Bitmap> Images { get; set; }
    }
}
