﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ModNote.ModNote {
    [Serializable]
    public class LearningObjective : ModNoteObject {
        public string Description { get; set; }
    }
}
