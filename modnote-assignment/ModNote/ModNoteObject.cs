﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ModNote.ModNote {
    [Serializable]
    public abstract class ModNoteObject {
        private Guid guid;
        public Guid Guid {
            get {
                if (guid == Guid.Empty) 
                    guid = Guid.NewGuid();
                return guid;
            }
        }

        public virtual void AddNote(Note note) { }

        public virtual void RemoveNote(Guid guid) { }
    }
}
