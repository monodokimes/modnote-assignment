﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ModNote.ModNote {
    [Serializable]
    public class Assignment : ModNoteObject {
        public string Description { get; set; }
        public DateTime DueDate { get; set; }
        public List<Note> Notes { get; set; }

        public override void AddNote(Note note) {
            Notes.Add(note);
        }

        public override void RemoveNote(Guid guid) {
            Notes = Notes.Where(n => n.Guid != guid).ToList();
        }
    }
}
