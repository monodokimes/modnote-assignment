﻿using ModNote;
using ModNote.ModNote;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace modnote_assignment {
    public partial class MainForm : Form {

        public List<Module> Modules { get; set; }

        private ModNoteManager _manager;
        private const string Module = "Module";
        private const string Assignment = "Assignment";

        private string _lastSelected = "";

        public MainForm() {
            InitializeComponent();
            _manager = ModNoteManager.Instance;
            Modules = _manager.Modules;
        }

        #region Events
        private void Form1_Load(object sender, EventArgs e) {
            Modules.ForEach(m => AddModuleToListView(m));
        }

        private void addModuleButton_Click(object sender, EventArgs e) {
            Module module = new Module();

            var addForm = new AddModuleForm();
            AddForm(addForm, () =>
                module = new Module {
                    Title = addForm.Title,
                    Code = addForm.Code,
                    Synopsis = addForm.Synopsis,
                    Assignments = new List<Assignment>(),
                    Notes = new List<Note>(),
                    LearningObjectives = new List<LearningObjective>()
                });

            if (!string.IsNullOrEmpty(module.Title)) {
                Modules.Add(module);
                AddModuleToListView(module);
            }
        }

        private void removeModuleButton_Click(object sender, EventArgs e) {
            var module = GetSelectedModule();

            if (module != null)
                RemoveModule(module);
        }

        private void addAssignmentButton_Click(object sender, EventArgs e) {
            var selectedModule = GetSelectedModule();
            if (selectedModule == null)
                return;

            var addForm = new AddAssignmentForm();
            AddForm(addForm, () => {
                var assignment = new Assignment {
                    DueDate = addForm.DueDate,
                    Description = addForm.Description,
                    Notes = new List<Note>()
                };
                selectedModule.Assignments.Add(assignment);
                AddAssignmentToListView(assignment);
            });
        }

        private void removeAssignmentButton_Click(object sender, EventArgs e) {
            var assignment = GetSelectedAssignment();

            if (assignment != null) {
                RemoveAssignment(assignment);

                notesListView.Items.Clear();
            }
        }

        private void addNoteButton_Click(object sender, EventArgs e) {
            ModNoteObject selectedObject = GetSelectedObject();

            var addForm = new AddNoteForm();
            AddForm(addForm, () => {
                var note = new Note {
                    Title = addForm.Title,
                    Text = addForm.NoteText,
                    Links = new List<string> { addForm.Links },
                    Images = new List<Bitmap>()
                };
                selectedObject.AddNote(note);
                AddNoteToListView(note);
            });
        }

        private void removeNoteButton_Click(object sender, EventArgs e) {
            var selectedObject = GetSelectedObject();
            if (notesListView.SelectedItems.Count > 0) {
                var selectedNote = notesListView.SelectedItems[0];

                selectedObject.RemoveNote(Guid.Parse(selectedNote.Name));
                notesListView.Items.RemoveByKey(selectedNote.Name);
            }
            
        }

        private void moduleListView_SelectedIndexChanged(object sender, EventArgs e) {
            _lastSelected = Module;

            var selectedModule = GetSelectedModule();

            assignmentListView.Items.Clear();
            notesListView.Items.Clear();

            if (selectedModule != null) {

                if (selectedModule.Assignments.Count > 0) {
                    selectedModule
                        .Assignments
                        .ForEach(a => AddAssignmentToListView(a));
                }

                if (selectedModule.Notes.Count > 0) {
                    selectedModule
                        .Notes
                        .ForEach(n => AddNoteToListView(n));
                }
            }
        }

        private void assignmentListView_SelectedIndexChanged(object sender, EventArgs e) {
            _lastSelected = Assignment;

            var selectedAssignment = GetSelectedAssignment();

            notesListView.Items.Clear();

            if (selectedAssignment != null) {
                if (selectedAssignment.Notes.Count > 0) {
                    selectedAssignment
                        .Notes
                        .ForEach(n => AddNoteToListView(n));
                }
            }
        }

        private void saveButton_Click(object sender, EventArgs e) {
            _manager.SetModules(Modules);
            _manager.SaveModules();
        }
        #endregion

        private ModNoteObject GetSelectedObject() {
            var selectedObjects = new ModNoteObject[] {
                GetSelectedModule(),
                GetSelectedAssignment()
            };

            var selectedObject = _lastSelected == Module ? selectedObjects[0] : selectedObjects[1];
            return selectedObject;
        }

        private void AddForm(Form addForm, Action action) {
            using (var form = addForm)
                if (form.ShowDialog() == DialogResult.OK)
                    action.Invoke();
        }

        private void AddNoteToListView(Note note) {
            AddItemToListView(
                notesListView,
                new ListViewItem(new[] {
                    note.Title,
                    note.Text,
                    note.Links.Count.ToString(),
                    note.Images.Count.ToString()
                }),
                note.Guid.ToString());
        }

        private void AddAssignmentToListView(Assignment assignment) {
            AddItemToListView(
                assignmentListView,
                new ListViewItem(new[] {
                    assignment.DueDate.ToShortDateString(),
                    assignment.Description
                }),
                assignment.Guid.ToString());
        }

        private void AddModuleToListView(Module module) {
            AddItemToListView(
                moduleListView,
                new ListViewItem(new[] {
                    module.Code,
                    module.Title,
                    module.Synopsis }),
                module.Code);
        }

        private void AddItemToListView(ListView listView, ListViewItem item, string itemName) {
            item.Name = itemName;
            listView.Items.Add(item);
        }

        private void RemoveModule(Module module) {
            RemoveModule(module.Code);
        }

        private void RemoveModule(string code) {
            Modules
                .Remove(Modules.Where(m => m.Code == code)
                .FirstOrDefault());

            moduleListView.Items.RemoveByKey(code);
        }

        private void RemoveAssignment(Assignment assignment) {
            RemoveAssignment(assignment.Guid.ToString());
        }

        private void RemoveAssignment(string assignmentId) {
            var selectedModule = GetSelectedModule();
            selectedModule
                .Assignments
                .Remove(selectedModule
                    .Assignments
                    .Where(a => a.Guid.ToString() == assignmentId)
                .FirstOrDefault());

            assignmentListView.Items.RemoveByKey(assignmentId);
        }

        private bool HasSelectedItems(ListView listView) {
            return listView.Items.Count > 0 && listView.SelectedItems.Count > 0;
        }

        private Module GetAssignmentModule(Assignment assignment) {
            return GetAssignmentModule(assignment.Guid.ToString());
        }

        private Module GetAssignmentModule(string assignmentId) {
            return Modules
                .Where(m => m
                    .Assignments
                    .Where(a => a.Guid.ToString() == assignmentId).Count() > 0)
                .FirstOrDefault();
        }

        private Module GetSelectedModule() {
            if (HasSelectedItems(moduleListView)) {
                ListViewItem listViewItem = GetSelectedListViewItem(moduleListView);

                return Modules
                    .Where(m => m.Code == listViewItem.Name)
                    .FirstOrDefault();
            }
            return null;
        }

        private Assignment GetSelectedAssignment() {
            if (HasSelectedItems(assignmentListView)) {
                var listViewItem = GetSelectedListViewItem(assignmentListView);

                var module = GetAssignmentModule(listViewItem.Name);

                return module
                    .Assignments
                    .Where(a => a.Guid.ToString() == listViewItem.Name)
                    .FirstOrDefault();
            }
            return null;
        }

        private ListViewItem GetSelectedListViewItem(ListView listView) {
            var selectedIndex = listView.SelectedIndices[0];

            return listView.Items[selectedIndex];
        }
    }
}
