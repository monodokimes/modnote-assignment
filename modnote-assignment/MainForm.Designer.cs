﻿namespace modnote_assignment {
    partial class MainForm {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing) {
            if (disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent() {
            this.panel1 = new System.Windows.Forms.Panel();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.notesListView = new System.Windows.Forms.ListView();
            this.columnHeader3 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader4 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader5 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader6 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.assignmentListView = new System.Windows.Forms.ListView();
            this.columnHeader1 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader2 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.label1 = new System.Windows.Forms.Label();
            this.moduleListView = new System.Windows.Forms.ListView();
            this.codeColumn = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.titleColumn = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.synopsisColumn = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.panel2 = new System.Windows.Forms.Panel();
            this.removeAssignmentButton = new System.Windows.Forms.Button();
            this.addAssignmentButton = new System.Windows.Forms.Button();
            this.removeModuleButton = new System.Windows.Forms.Button();
            this.addModuleButton = new System.Windows.Forms.Button();
            this.removeNoteButton = new System.Windows.Forms.Button();
            this.addNoteButton = new System.Windows.Forms.Button();
            this.saveButton = new System.Windows.Forms.Button();
            this.panel1.SuspendLayout();
            this.panel2.SuspendLayout();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.label3);
            this.panel1.Controls.Add(this.label2);
            this.panel1.Controls.Add(this.notesListView);
            this.panel1.Controls.Add(this.assignmentListView);
            this.panel1.Controls.Add(this.label1);
            this.panel1.Controls.Add(this.moduleListView);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(555, 545);
            this.panel1.TabIndex = 0;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(15, 364);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(35, 13);
            this.label3.TabIndex = 5;
            this.label3.Text = "Notes";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(15, 187);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(66, 13);
            this.label2.TabIndex = 4;
            this.label2.Text = "Assignments";
            // 
            // notesListView
            // 
            this.notesListView.Alignment = System.Windows.Forms.ListViewAlignment.SnapToGrid;
            this.notesListView.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.columnHeader3,
            this.columnHeader4,
            this.columnHeader5,
            this.columnHeader6});
            this.notesListView.FullRowSelect = true;
            this.notesListView.GridLines = true;
            this.notesListView.Location = new System.Drawing.Point(12, 379);
            this.notesListView.Name = "notesListView";
            this.notesListView.Size = new System.Drawing.Size(531, 154);
            this.notesListView.TabIndex = 3;
            this.notesListView.UseCompatibleStateImageBehavior = false;
            this.notesListView.View = System.Windows.Forms.View.Details;
            // 
            // columnHeader3
            // 
            this.columnHeader3.Text = "Title";
            this.columnHeader3.Width = 86;
            // 
            // columnHeader4
            // 
            this.columnHeader4.Text = "Text";
            this.columnHeader4.Width = 342;
            // 
            // columnHeader5
            // 
            this.columnHeader5.Text = "Links";
            this.columnHeader5.Width = 45;
            // 
            // columnHeader6
            // 
            this.columnHeader6.Text = "Images";
            this.columnHeader6.Width = 50;
            // 
            // assignmentListView
            // 
            this.assignmentListView.Alignment = System.Windows.Forms.ListViewAlignment.SnapToGrid;
            this.assignmentListView.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.columnHeader1,
            this.columnHeader2});
            this.assignmentListView.FullRowSelect = true;
            this.assignmentListView.GridLines = true;
            this.assignmentListView.Location = new System.Drawing.Point(12, 203);
            this.assignmentListView.Name = "assignmentListView";
            this.assignmentListView.Size = new System.Drawing.Size(531, 154);
            this.assignmentListView.TabIndex = 2;
            this.assignmentListView.UseCompatibleStateImageBehavior = false;
            this.assignmentListView.View = System.Windows.Forms.View.Details;
            this.assignmentListView.SelectedIndexChanged += new System.EventHandler(this.assignmentListView_SelectedIndexChanged);
            // 
            // columnHeader1
            // 
            this.columnHeader1.Text = "Date Due";
            this.columnHeader1.Width = 86;
            // 
            // columnHeader2
            // 
            this.columnHeader2.Text = "Description";
            this.columnHeader2.Width = 436;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(12, 9);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(47, 13);
            this.label1.TabIndex = 1;
            this.label1.Text = "Modules";
            // 
            // moduleListView
            // 
            this.moduleListView.Alignment = System.Windows.Forms.ListViewAlignment.SnapToGrid;
            this.moduleListView.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.codeColumn,
            this.titleColumn,
            this.synopsisColumn});
            this.moduleListView.FullRowSelect = true;
            this.moduleListView.GridLines = true;
            this.moduleListView.Location = new System.Drawing.Point(12, 25);
            this.moduleListView.Name = "moduleListView";
            this.moduleListView.Size = new System.Drawing.Size(531, 154);
            this.moduleListView.TabIndex = 0;
            this.moduleListView.UseCompatibleStateImageBehavior = false;
            this.moduleListView.View = System.Windows.Forms.View.Details;
            this.moduleListView.SelectedIndexChanged += new System.EventHandler(this.moduleListView_SelectedIndexChanged);
            // 
            // codeColumn
            // 
            this.codeColumn.Text = "Code";
            this.codeColumn.Width = 102;
            // 
            // titleColumn
            // 
            this.titleColumn.Text = "Title";
            this.titleColumn.Width = 154;
            // 
            // synopsisColumn
            // 
            this.synopsisColumn.Text = "Synopsis";
            this.synopsisColumn.Width = 359;
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.saveButton);
            this.panel2.Controls.Add(this.removeNoteButton);
            this.panel2.Controls.Add(this.addNoteButton);
            this.panel2.Controls.Add(this.removeAssignmentButton);
            this.panel2.Controls.Add(this.addAssignmentButton);
            this.panel2.Controls.Add(this.removeModuleButton);
            this.panel2.Controls.Add(this.addModuleButton);
            this.panel2.Dock = System.Windows.Forms.DockStyle.Right;
            this.panel2.Location = new System.Drawing.Point(561, 0);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(181, 545);
            this.panel2.TabIndex = 1;
            // 
            // removeAssignmentButton
            // 
            this.removeAssignmentButton.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.removeAssignmentButton.Location = new System.Drawing.Point(3, 279);
            this.removeAssignmentButton.Name = "removeAssignmentButton";
            this.removeAssignmentButton.Size = new System.Drawing.Size(175, 23);
            this.removeAssignmentButton.TabIndex = 3;
            this.removeAssignmentButton.Text = "Remove Assignment";
            this.removeAssignmentButton.UseVisualStyleBackColor = true;
            this.removeAssignmentButton.Click += new System.EventHandler(this.removeAssignmentButton_Click);
            // 
            // addAssignmentButton
            // 
            this.addAssignmentButton.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.addAssignmentButton.Location = new System.Drawing.Point(3, 250);
            this.addAssignmentButton.Name = "addAssignmentButton";
            this.addAssignmentButton.Size = new System.Drawing.Size(175, 23);
            this.addAssignmentButton.TabIndex = 2;
            this.addAssignmentButton.Text = "Add Assignment";
            this.addAssignmentButton.UseVisualStyleBackColor = true;
            this.addAssignmentButton.Click += new System.EventHandler(this.addAssignmentButton_Click);
            // 
            // removeModuleButton
            // 
            this.removeModuleButton.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.removeModuleButton.Location = new System.Drawing.Point(3, 103);
            this.removeModuleButton.Name = "removeModuleButton";
            this.removeModuleButton.Size = new System.Drawing.Size(175, 23);
            this.removeModuleButton.TabIndex = 1;
            this.removeModuleButton.Text = "Remove Module";
            this.removeModuleButton.UseVisualStyleBackColor = true;
            this.removeModuleButton.Click += new System.EventHandler(this.removeModuleButton_Click);
            // 
            // addModuleButton
            // 
            this.addModuleButton.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.addModuleButton.Location = new System.Drawing.Point(3, 74);
            this.addModuleButton.Name = "addModuleButton";
            this.addModuleButton.Size = new System.Drawing.Size(175, 23);
            this.addModuleButton.TabIndex = 0;
            this.addModuleButton.Text = "Add Module";
            this.addModuleButton.UseVisualStyleBackColor = true;
            this.addModuleButton.Click += new System.EventHandler(this.addModuleButton_Click);
            // 
            // removeNoteButton
            // 
            this.removeNoteButton.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.removeNoteButton.Location = new System.Drawing.Point(3, 451);
            this.removeNoteButton.Name = "removeNoteButton";
            this.removeNoteButton.Size = new System.Drawing.Size(175, 23);
            this.removeNoteButton.TabIndex = 5;
            this.removeNoteButton.Text = "Remove Note";
            this.removeNoteButton.UseVisualStyleBackColor = true;
            this.removeNoteButton.Click += new System.EventHandler(this.removeNoteButton_Click);
            // 
            // addNoteButton
            // 
            this.addNoteButton.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.addNoteButton.Location = new System.Drawing.Point(3, 422);
            this.addNoteButton.Name = "addNoteButton";
            this.addNoteButton.Size = new System.Drawing.Size(175, 23);
            this.addNoteButton.TabIndex = 4;
            this.addNoteButton.Text = "Add Note";
            this.addNoteButton.UseVisualStyleBackColor = true;
            this.addNoteButton.Click += new System.EventHandler(this.addNoteButton_Click);
            // 
            // saveButton
            // 
            this.saveButton.Location = new System.Drawing.Point(50, 500);
            this.saveButton.Name = "saveButton";
            this.saveButton.Size = new System.Drawing.Size(75, 23);
            this.saveButton.TabIndex = 6;
            this.saveButton.Text = "Save Data";
            this.saveButton.UseVisualStyleBackColor = true;
            this.saveButton.Click += new System.EventHandler(this.saveButton_Click);
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(742, 545);
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.panel1);
            this.Name = "MainForm";
            this.Text = "Form1";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.panel2.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.ListView moduleListView;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.ColumnHeader codeColumn;
        private System.Windows.Forms.ColumnHeader titleColumn;
        private System.Windows.Forms.Button removeModuleButton;
        private System.Windows.Forms.Button addModuleButton;
        private System.Windows.Forms.ColumnHeader synopsisColumn;
        private System.Windows.Forms.ListView assignmentListView;
        private System.Windows.Forms.ColumnHeader columnHeader1;
        private System.Windows.Forms.ColumnHeader columnHeader2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.ListView notesListView;
        private System.Windows.Forms.ColumnHeader columnHeader3;
        private System.Windows.Forms.ColumnHeader columnHeader4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.ColumnHeader columnHeader5;
        private System.Windows.Forms.ColumnHeader columnHeader6;
        private System.Windows.Forms.Button removeAssignmentButton;
        private System.Windows.Forms.Button addAssignmentButton;
        private System.Windows.Forms.Button removeNoteButton;
        private System.Windows.Forms.Button addNoteButton;
        private System.Windows.Forms.Button saveButton;
    }
}

