﻿using ModNote.ModNote;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace modnote_assignment {
    public class TextDataManager {
        private List<Module> _modules = new List<Module>();
        public List<Module> Modules { get { return _modules; } }

        private IEnumerable<string> _dataPaths;

        public const string Directory = "../../Data/";
        public const string CodeTitle = "CODE";
        public const string TitleTitle = "TITLE";
        public const string SynopsisTitle = "SYNOPSIS";
        public const string LoTitleTemplate = "LO{0}";
        public const string AssignmentTitle = "ASSIGNMENT";

        public TextDataManager() {
            _dataPaths = System.IO.Directory.GetFiles(Directory);
        }

        public void LoadData() {
            var moduleData = new List<string[]>();

            foreach (var path in _dataPaths)
                moduleData.Add(GetDataFromPath(path));

            foreach (var moduleDatum in moduleData)
                _modules.Add(CreateModule(moduleDatum));
        }

        private Module CreateModule(string[] moduleDatum) {
            return new Module {
                Code = GetLineBelowTitle(moduleDatum, CodeTitle),
                Title = GetLineBelowTitle(moduleDatum, TitleTitle),
                Synopsis = GetLineBelowTitle(moduleDatum, SynopsisTitle),
                LearningObjectives = GetLearningObjectives(moduleDatum).ToList(),
                Assignments = GetAssignments(moduleDatum).ToList(),
                Notes = new List<Note>()
            };
        }

        private IEnumerable<Assignment> GetAssignments(string[] moduleDatum) {
            return moduleDatum
                .Where(l => 
                    Array.IndexOf(moduleDatum, l) > Array.IndexOf(moduleDatum, moduleDatum
                        .Where(li => li.StartsWith(AssignmentTitle))
                        .FirstOrDefault())
                )
                .Select(a => {
                    var assignmentData = a.Split('\t');
                    var dateData = assignmentData[1];

                    return new Assignment {
                        Description = assignmentData[0],
                        DueDate = dateData.StartsWith("w/c") ? DateTime.Parse(dateData.Substring(4)) : DateTime.Parse(dateData),
                        Notes = new List<Note>()
                    };
                });
        }

        private IEnumerable<LearningObjective> GetLearningObjectives(string[] moduleDatum) {
            return moduleDatum
                .ToList()
                .Where(l => l.StartsWith(string.Format(LoTitleTemplate, "")))
                .Where(lod => lod.Length > LoTitleTemplate.Length)
                .Select(lod => new LearningObjective {
                    Description = lod.Substring(LoTitleTemplate.Length - 1)
                });
        }

        private string GetLineBelowTitle(string[] array, string title) {
            return array
                .Where(i => i == title)
                .Select(l => array[Array.IndexOf(array, title) + 1])
                .FirstOrDefault();
        }

        private string[] GetDataFromPath(string path) {
            return System.IO.File.ReadAllLines(path);
        }
    }
}