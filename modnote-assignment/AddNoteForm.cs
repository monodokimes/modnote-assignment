﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace modnote_assignment {
    public partial class AddNoteForm : Form {

        public string Title { get; set; }
        public string NoteText { get; set; }
        public string Links { get; set; }

        public AddNoteForm() {
            InitializeComponent();
        }

        private void okButton_Click(object sender, EventArgs e) {
            Title = titleTextBox.Text;
            NoteText = textTextBox.Text;
            Links = linksTextBox.Text;

            DialogResult = DialogResult.OK;
        }
    }
}
